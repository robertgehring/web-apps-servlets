/* Robert Gehring (UTEID: rjg2358) (CS Username: rgehring) */
/* Modern Web Apps Spring 2015 - Assignment 2              */  

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Cookie;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.Iterator;
import java.util.SortedSet;
import java.util.TreeSet;


public class MyEavesdropServlet extends HttpServlet
{
	private static final long serialVersionUID = 1L;
    
    @SuppressWarnings("null")
	@Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) 
                    throws ServletException, IOException
    {
    	String type = request.getParameter("type");
    	String project = request.getParameter("project");
    	String year = request.getParameter("year");
    	String username = request.getParameter("username");
    	String session = request.getParameter("session");
    	
    	Cookie[] cookies = request.getCookies();
    	
    	boolean meetingLogsRequest = !(type==null || project==null || year==null);
    	boolean channelLogsRequest = !(type==null || project==null);
    	boolean goodRequestParameters = meetingLogsRequest || channelLogsRequest;
    	
    	if (goodRequestParameters) /* Call intends to make a request */
    	{	
    		Document doc = getEavesdropDocument(response, type, project, year);
    		
    		if(doc!=null) /* Good Eavesdrop parameters */
    		{
    			if(session!=null) /* Handle the session parameter */
    			{
    				if(session.toLowerCase().equals("end")) /* User seeks to end session at the end of job */
    				{
    					if(cookies==null) /* No session to end */
    						response.getWriter().println("Error: Attempted to end a non-existing session!");
    					else /* Session exists: Handle request and kill cookies at the end to stop the session */
    					{
    						printSessionHistory(response, cookies);
    						queryEavesdrop(response, doc);
    						killCookies(response, cookies);
    					}
    				}
    				else if(session.toLowerCase().equals("start")) /* User seeks to start session before request */
    				{
    					if(cookies!=null) /* Session already exists */
    						response.getWriter().println("Error: Attmpted to start a session that already exists!");
    					else /* Session needs to be made and request needs to be made */
    					{
    						String path = request.getRequestURL().toString();
    						addSessionCookie(response, "Username", username, path); /* Init Session */
    						queryEavesdrop(response, doc); /* Do request */
    					}
    				}
    				else /* Session parameter is bad */
    					response.getWriter().println("Error: Session parameter is bad. Should either be end or start!");
    			}
    			else /* Do call - Print session history if a session is being tracked */
    			{
    				if(cookies!=null) /* A session is being tracked */
    				{
    					printSessionHistory(response, cookies);
    					addSessionCookie(response, ""+cookies.length, getUrlReconstructed(request), request.getRequestURL().toString());
    				}
    				queryEavesdrop(response, doc);
    			}
    		}
    	}
    	else /* No eavesdrop request info combo */
    	{
    		if(session!=null) /* Handle the session parameter */
    		{
    			if(session.toLowerCase().equals("start"))
    			{
    				if(cookies!=null) /* Session already being tracked */
    					response.getWriter().println("Error: Attempted to start a session that is already being tacked!");
    				else /* Initiate Session */
    					addSessionCookie(response, "Username", username, request.getRequestURL().toString());
    			}
    			else if(session.toLowerCase().equals("end"))
    			{
    				if(cookies==null) /* Session is not being tracked */
    					response.getWriter().println("Error: Attemped to end a session that doesn't exist!");
    				killCookies(response,cookies);
    			}
    			else
    				response.getWriter().println("Error: Session parameter is bad. Should either be start or end!");	
    		}
    	} 
    }
    
    /* Adds this interaction by creating a cookie representing it and giving it to reponse */
    private static void addSessionCookie(HttpServletResponse response, String cookieId, String cookieValue, String path)
    {   
    	/* Add a cookie */
    	Cookie thisInteraction = new Cookie(""+cookieId, cookieValue );
    	thisInteraction.setMaxAge(60*60*24); //24 hour age
    	thisInteraction.setPath(path);
    	response.addCookie(thisInteraction);
    }
    
    /* Reconstruct the complete url path request sent to the servlet */
    private String getUrlReconstructed(HttpServletRequest request)
    {
        StringBuffer requestURL = request.getRequestURL();
        String queryString = request.getQueryString();

        if (queryString == null) {
            return requestURL.toString();
        } else {
            return requestURL.append('?').append(queryString).toString();
        }
    }
    
    /* Goes through the cookies to get the previous interaction history and prints it to stdout */
    private static void printSessionHistory(HttpServletResponse response, Cookie[] cookies) throws IOException
    {
    	SortedSet<Cookie> cookiesSorted = new TreeSet<Cookie>(new CookieComparator());
    	
    	
    	
    	if(cookies!=null && cookies.length > 1)
    	{
    		response.getWriter().println("Visited Urls\n"); /* Do we output this for first query? */
    		Cookie cookie;
    		/* Sort the cookies by order received with respect to time */
    		for(Cookie c:cookies)
    		{
    			try
    			{
    				Integer.parseInt(c.getName());
    				cookiesSorted.add(c);
    			}
    			catch (Exception e) {/* Do Nothing */}
    		}
    		
    		/* Iterate thru to get the history of visited URLs */
    		Iterator<Cookie> cookiesSortedIterator = cookiesSorted.iterator();
    		
    		while(cookiesSortedIterator.hasNext())
    		{
    			cookie = cookiesSortedIterator.next();
    			response.getWriter().println(cookie.getName() + ":  " + cookie.getValue() + "\n");
    		}
    		//response.getWriter().print("\n");
    	}
    }
    
    /* To be enacted when "end" is passed as a parameter. Will fulfill a request if there is one and delete all cookies */
    private static void killCookies(HttpServletResponse response, Cookie[] cookies)
    {    	
    	if(cookies!=null)
    	{
    		/* Kill cookies */
    		for(Cookie c:cookies)
    		{
    			c.setMaxAge(0);
    			response.addCookie(c);
    		}
    	}
    }
    
    /* Get the data from eavesdrop for a meeting request */
    private static void queryEavesdrop(HttpServletResponse response, Document doc) throws IOException
    {
    	Elements links = doc.select("a[href]");
    	//response.getWriter().println(links.toString());
    		
    	for(int i=5;i<links.size();i++)
    	{
    		String[] linkSplit = links.get(i).toString().split(">");
    		String linkToShow = linkSplit[linkSplit.length-1];
    		linkToShow = linkToShow.split("<")[0];
    		response.getWriter().println(linkToShow);
    	}
    }
    
    /* Try to open the eavesdrop page with the specified parameters */
    /* Return a Document that represents the data if it exists or   */
    /* null if the page can not be resolved                         */
    private static Document getEavesdropDocument(HttpServletResponse response, String type, String project, String year) throws IOException
    {	
    	String urlStringProjectTest = null;
    	String urlStringFull = null;
    	String errorParameter;
    	
    	if(! (type.toLowerCase().equals("meetings") || type.toLowerCase().equals("irclogs")) ) /* Type parameter is bad */
    	{
    		response.getWriter().println("Disallowed value specified for parameter type");
    		return null;
    	}
    	
    	if(year==null) /* Channel request */
    	{
    		urlStringFull = "http://eavesdrop.openstack.org/" + URLEncoder.encode(type,"UTF-8" ) + "/" + URLEncoder.encode(project,"UTF-8" ) + "/";
    		errorParameter = "project";
    	}
    	else /* Meeting request */
    	{
    		urlStringProjectTest = "http://eavesdrop.openstack.org/" + URLEncoder.encode(type,"UTF-8" ) + "/" + URLEncoder.encode(project,"UTF-8" ) + "/";
    		urlStringFull = "http://eavesdrop.openstack.org/" + URLEncoder.encode(type,"UTF-8" ) + "/" + URLEncoder.encode(project,"UTF-8" ) + "/" + URLEncoder.encode(year,"UTF-8" ) + "/";
    		errorParameter = "year";
    		
    		try /* Check the validity of the project parameter */
    		{
    			Jsoup.connect(urlStringProjectTest).get();
    		}
    		catch(Exception e)
    		{
    			response.getWriter().println("Disallowed value specified for parameter project");
    			return null;
    		}
    	}
    	
    	
    	try /* Either test the validity of the project or the year parameter */
    	{
    		return Jsoup.connect(urlStringFull).get(); /* The handle for the webpage is returned */
    	}
    	catch(Exception e)
    	{
    		response.getWriter().println("Disallowed value specified for parameter " + errorParameter);
    		return null;
    	}
    }
}

