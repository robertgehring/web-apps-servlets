/* Robert Gehring (UTEID: rjg2358) (CS Username: rgehring) */
/* Modern Web Apps Spring 2015 - Assignment 2              */

import java.util.Comparator;

import javax.servlet.http.Cookie;

    public class CookieComparator implements Comparator<Cookie>
    {
        public int compare(Cookie c1, Cookie c2)
        {
            int result = Integer.parseInt(c1.getName()) - Integer.parseInt(c2.getName());
            return result;
        }
    }